<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('homepage');
});

Route::get('/about', function () {
    return view('about', [
        "name" => "Aldo Ramadhan",
        "email" => "ARUDORAMA13@GMAIL.COM",
        "image" => "/assets/images/FB_IMG_1630578812258.jpg"
    ]);
});

Route::get('/portofolio', function () {
    return view('portofolio');
});
