<div class="container">
    <div class="row cols-1">
        <!-- Blog entries-->
        <div class="col-lg-8">
            <!-- Featured blog post-->
            <div class="card mb-4">
                <a href="#!"><img class="card-img-top" src="/assets/images/Gambar1.jpg" alt="..." /></a>
                <div class="card-body">
                    <div class="small text-muted">January 1, 2022</div>
                    <h2 class="card-title">Anime "Chainshaw Man" Confirmed Release Date Airing</h2>
                    <p class="card-text"></p>
                    <a class="btn btn-primary" href="#!">Read more →</a>
                </div>
            </div>
            <!-- Nested row for non-featured blog posts-->
            <div class="row">
                <h3 class="fw-bolder">Anime</h3>
                <div class="col-lg-6">
                    <!-- Blog post-->
                    <div class="card mb-4">
                        <a href="#!"><img class="card-img-top" src="/assets/images/Gambar3.jpg"
                                alt="..." /></a>
                        <div class="card-body">
                            <div class="small text-muted">January 1, 2022</div>
                            <h2 class="card-title h4">"Jujutsu Kaisen" Confirmed Get TV Anime Season 2</h2>
                            <p class="card-text"></p>
                            <a class="btn btn-primary" href="#!">Read more →</a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <!-- Blog post-->
                    <div class="card mb-4">
                        <a href="#!"><img class="card-img-top" src="/assets/images/Gambar4.jpg"
                                alt="..." /></a>
                        <div class="card-body">
                            <div class="small text-muted">January 1, 2022</div>
                            <h2 class="card-title h4">{{ $post }}</h2>
                            <p class="card-text"> Lorem ipsum dolor sit amet, consectetur adipisicing elit. Maxime
                                distinctio illo velit. Modi dicta praesentium suscipit. Nulla ut, unde, sed nisi quasi
                                nobis blanditiis totam vitae aliquam magnam, cupiditate a. Fugit ducimus laborum quod
                                voluptate qui maiores expedita? Voluptate, cumque. Repellendus, deleniti tempore
                                dignissimos voluptates eaque magni minus doloribus officia consequatur, quia
                                consequuntur similique, in voluptas vitae voluptate repellat modi nulla mollitia! Alias
                                aspernatur beatae repudiandae sapiente incidunt, commodi ducimus voluptas quia tempora?
                                Exercitationem maiores nobis perspiciatis quam libero voluptate.</p>
                            <a class="btn btn-primary" href="#!">Read more →</a>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Nested row for non-featured blog posts-->
            <div class="row">
                <h3 class="fw-bolder">Game</h3>
                <div class="col-lg-6">
                    <!-- Blog post-->
                    <div class="card mb-4">
                        <a href="#!"><img class="card-img-top" src="/assets/images/Gambar5.jpg"
                                alt="..." /></a>
                        <div class="card-body">
                            <div class="small text-muted">January 1, 2022</div>
                            <h2 class="card-title h4">Ubisoft Confirmed New Game for Assassins Creeds "Assassins Creeds
                                Mirage"</h2>
                            <p class="card-text"></p>
                            <a class="btn btn-primary" href="#!">Read more →</a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <!-- Blog post-->
                    <div class="card mb-4">
                        <a href="#!"><img class="card-img-top" src="/assets/images/Gambar6.png"
                                alt="..." /></a>
                        <div class="card-body">
                            <div class="small text-muted">January 1, 2022</div>
                            <h2 class="card-title h4">Rockstar Games is Reportedly Under Development of GTA VI</h2>
                            <p class="card-text"></p>
                            <a class="btn btn-primary" href="#!">Read more →</a>
                        </div>
                    </div>
                </div>
            </div>
