<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- CSS -->
    <link href="{{ asset('/css/style.css') }}" rel="stylesheet">
    <link href="{{ asset('/css/bootstrap.css') }}" rel="stylesheet">
    <title>Welcome | My Blog</title>
</head>

<body>
    <!-- Responsive navbar-->
    @include('Partial.navbar')
    <!-- Header-->
    <div>
        @include('Partial.header')
    </div>
    <!--End of Header-->

    <!-- Page header with logo and tagline-->
    <headline class="py-5 bg-light border-bottom mb-4">
        <div class="text-center my-5">
            <h1 class="fw-bolder">Most Popular Article</h1>
        </div>
    </headline>
    <!-- Page content-->
    <div>
        @include('Partial.content')
    </div>

    <!-- End of Page content-->
    <!-- Side widgets-->

    @include('Partial.widget')

    <!-- End of Widgets-->

    <!-- Pagination-->
    <nav aria-label="Pagination">
        <hr class="my-0" />
        <ul class="pagination justify-content-center my-4">
            <li class="page-item disabled"><a class="page-link" href="#" tabindex="-1"
                    aria-disabled="true">Newer</a></li>
            <li class="page-item active" aria-current="page"><a class="page-link" href="#!">1</a></li>
            <li class="page-item"><a class="page-link" href="#!">2</a></li>
            <li class="page-item"><a class="page-link" href="#!">3</a></li>
            <li class="page-item disabled"><a class="page-link" href="#!">...</a></li>
            <li class="page-item"><a class="page-link" href="#!">5</a></li>
            <li class="page-item"><a class="page-link" href="#!">Older</a></li>
        </ul>
    </nav>

    </div>
    </div>
    </div>

    </div>
    <!-- Footer-->
    <div>
        @include('Partial.footer')
    </div>

    <!-- End of Footer-->

    <!-- Bootstrap core JS-->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"></script>
    <!-- Core theme JS-->
    <script src="/js/scripts.js"></script>
</body>

</html>
