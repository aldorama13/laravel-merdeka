<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>About Me | My Blog</title>
</head>

<body>
    <h1>About Me</h1>
    <h2>{{ $name }}</h2>
    <p>{{ $email }}</p>
    <img src={{ $image }} alt={{ $name }} width="200">
</body>

</html>
